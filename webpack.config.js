const webpack = require("webpack");

module.exports = {
	entry: "./src/index.jsx",
	output: {
		path: __dirname,
		filename: "./assets/js/popup.js"
	},
	module: {
		loaders: [
			{
				test: /.jsx$/,
				loader: "babel-loader",
				exclude: /node_modules/,
				options: {
					presets: [["env", "react"]],
					plugins: ["transform-class-properties"]
				}
			},
			{ test: /\.css$/, loader: [
					'style-loader',
					'css-loader'
				]
			},
		],
	},
	plugins: [
		new webpack.optimize.UglifyJsPlugin({
			include: /\.min\.js$/,
			minimize: true
		})
	]
};

