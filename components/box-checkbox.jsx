import React, { Component } from 'react';

export default class BoxCheckbox extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            name: props.name,
            value: props.value
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        let state = this.state;
        state.value = !state.value;
        this.setState(state);
    }

    render () {
        let className = 'input-text regular-input';
        let checked = false;

        if ( this.state.value == 1 ) {
            checked = true;
        }

        return (
            <input
                className={className}
                type="checkbox"
                name={this.state.name}
                value="1"
                checked={checked}
                onChange={this.handleChange}
            />
        )
    }
}
