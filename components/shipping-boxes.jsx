import React, { Component } from 'react';
import BoxesTable from "./boxes-table.jsx";

export default class ShippingBoxes extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            boxes: JSON.parse(props.boxes),
            builtInBoxes: JSON.parse(props.builtinboxes),
            name: props.name,
            labels: JSON.parse(props.labels),
        };
    }

    render () {
        return (
            <div>
                <BoxesTable
                    boxes={this.state.boxes}
                    builtInBoxes={this.state.builtInBoxes}
                    name={this.state.name}
                    labels={this.state.labels}
                />
            </div>
        )
    }
}
