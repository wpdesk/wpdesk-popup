[![pipeline status](https://gitlab.com/wpdesk/wpdesk-popup/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wpdesk-popup/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wpdesk-popup/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wpdesk-popup/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wpdesk-popup/v/stable)](https://packagist.org/packages/wpdesk/wpdesk-popup) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wpdesk-popup/downloads)](https://packagist.org/packages/wpdesk/wpdesk-popup) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wpdesk-popup/v/unstable)](https://packagist.org/packages/wpdesk/wpdesk-popup) 
[![License](https://poser.pugx.org/wpdesk/wpdesk-popup/license)](https://packagist.org/packages/wpdesk/wpdesk-popup) 

# wpdesk-popup

Before commit execute: `npm run build`!
